﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="decrypt.aspx.cs" Inherits="_Default" Debug="true" %>
<html>
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="col-md-4">
            <asp:Panel ID="decryptPanel" runat="server" Visible="False">
                <h2>Decrypt/Download file</h2>
                <asp:Literal ID="results" runat="server" />
                <p>Once a file is downloaded, the encrypted file is destroyed,<br />and can not be downloaded again.</p>
                Password:
                <asp:TextBox ID="passworddecrypt" TextMode="Password" runat="server" />
                <asp:Button ID="Button2" runat="server" OnClick="decryptSubmit" Text="Decrypt" />
            </asp:Panel>
        </div>



    </form>
</body>
</html>
