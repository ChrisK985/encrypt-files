﻿using System;
using System.Web.Configuration;

public partial class _Encrypt : System.Web.UI.Page
{

    string encryptPath = WebConfigurationManager.AppSettings["encryptPath"];
    string uploadPath = WebConfigurationManager.AppSettings["uploadPath"];

    protected void Page_Load(object sender, EventArgs e)
    {
        //Add allowed IP addresses
        if (Request.IsLocal) { encryptPanel.Visible = true; }
    }

    protected void encryptSubmit(object sender, EventArgs e)
    {
        string guid = Guid.NewGuid().ToString();
        
        if (filesubmit.HasFile)
        {
            string inpath = Server.MapPath(uploadPath) + filesubmit.FileName;
            string outpath = Server.MapPath(encryptPath) + guid;
            Random r = new Random();
            string password = System.Web.Security.Membership.GeneratePassword(r.Next(15, 20), r.Next(5, 10));

            encryption.File f = new encryption.File();
            f.guid = guid;
            f.filename = filesubmit.FileName;
            f.passwordHash = password;

            try
            {
                filesubmit.PostedFile.SaveAs(inpath);
                encryption.Files.EncryptFile(inpath, outpath, f);
                results.Text = string.Format("<div class='alert alert-success'><h4>SUCCESS</h4>Download via this link:<br><a target='_blank' href='/file/decrypt.aspx?id={0}'>{1}/file/decrypt.aspx?id={0}</a><br><br>Password: {2}</div>", guid, Request.Url.Host.ToLower(), password);
            } catch(Exception ex)
            {
                results.Text = string.Format("<div class='alert alert-danger'><h4>FAILURE</h4>{0}</div>", ex.Message);
            }
            
            
        }
        else { results.Text = "Please select a file first."; }
    }
}