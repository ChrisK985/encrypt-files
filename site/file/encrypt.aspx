﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="encrypt.aspx.cs" Inherits="_Encrypt" Debug="true" %>

<html>
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
    
</head>
<body>
    <form id="form1" runat="server">
        <div class="col-md-4">
            
            <asp:Panel ID="encryptPanel" runat="server" Visible="False">
                <h2>Encrypt/Send file</h2>
                <asp:Literal ID="results" runat="server" />
                <asp:FileUpload ID="filesubmit" runat="server" /><br />
                <br />
                <asp:Button ID="Button1" OnClick="encryptSubmit" runat="server" Text="Encrypt" />
            </asp:Panel>
        </div>
    </form>
</body>
</html>
