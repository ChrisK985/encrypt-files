﻿using System;
using System.Web.Configuration;

public partial class _Default : System.Web.UI.Page
{

    string encryptPath = WebConfigurationManager.AppSettings["encryptPath"];
    string uploadPath = WebConfigurationManager.AppSettings["uploadPath"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["id"])) { decryptPanel.Visible = true; }
    }

    protected void decryptSubmit(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["id"].Trim()))
        {
            try{
                encryption.Files.DecryptAndDownloadFile(Server.MapPath(encryptPath), Server.MapPath(uploadPath), passworddecrypt.Text.Trim(), Request["id"].Trim());
            }
            catch (Exception ex)
            {
                results.Text = string.Format("<div class='alert alert-danger'><h4>FAILURE</h4>{0}</div>", ex.Message);
            }
            
        }
    }

}