﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace encryption
{
    public static class Files
    {
        private static string connName = "sqlconnection";
        public static void EncryptFile(string inputFile, string outputFile, File f)
        {
            FileStream readFile = new FileStream(inputFile, FileMode.Open, FileAccess.Read);
            FileStream writeFile = new FileStream(outputFile, FileMode.OpenOrCreate, FileAccess.Write);
            byte[] storage = new byte[4096];
            long fileWritten = 0;
            long totlen = readFile.Length;
            int bytesToWrite;

            writeFile.SetLength(0);
            try
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;
                    AES.Padding = PaddingMode.PKCS7;

                    f.passwordHash = hashString(f.passwordHash);
                    var key = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(f.passwordHash), Encoding.UTF8.GetBytes(f.guid + f.filename), 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(writeFile, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        while (fileWritten < totlen)
                        {
                            bytesToWrite = readFile.Read(storage, 0, 4096);
                            cs.Write(storage, 0, bytesToWrite);
                            fileWritten = fileWritten + bytesToWrite;
                        }
                    }
                }
                insert(f);


            }
            catch (Exception ex)
            {
                System.IO.File.Delete(outputFile);
                throw new Exception(ex.Message);
            }
            finally
            {
                readFile.Close();
                writeFile.Close();
                System.IO.File.Delete(inputFile);
            }


        }

        private static void downloadFile(string filepath, bool deleteAfter = false)
        {
            try
            {
                HttpContext.Current.Response.ContentType = "Application/octet-stream";
                HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(filepath));
                HttpContext.Current.Response.TransmitFile(filepath);
                HttpContext.Current.Response.Flush();
            }
            finally { if (deleteAfter) System.IO.File.Delete(filepath); }
            HttpContext.Current.Response.End();
        }

        public static void DecryptAndDownloadFile(string inputPath, string outputPath, string password, string guid)
        {
            byte[] storage = new byte[4096];
            long fileWritten = 0;
            int bytesToWrite;
            File f = getByPasswordAndGUID(hashString(password), guid);

            if (f != null)
            {
                FileStream readFile = new FileStream(inputPath + f.guid, FileMode.Open, FileAccess.Read);
                FileStream writeFile = new FileStream(outputPath + f.filename, FileMode.OpenOrCreate, FileAccess.Write);

                try
                {
                    long totlen = readFile.Length;
                    writeFile.SetLength(0);
                    using (RijndaelManaged AES = new RijndaelManaged())
                    {
                        AES.KeySize = 256;
                        AES.BlockSize = 128;
                        AES.Padding = PaddingMode.PKCS7;

                        var key = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(f.passwordHash), Encoding.UTF8.GetBytes(f.guid + f.filename), 1000);
                        AES.Key = key.GetBytes(AES.KeySize / 8);
                        AES.IV = key.GetBytes(AES.BlockSize / 8);

                        AES.Mode = CipherMode.CBC;

                        using (var cs = new CryptoStream(writeFile, AES.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            while (fileWritten < totlen)
                            {
                                bytesToWrite = readFile.Read(storage, 0, 4096);
                                cs.Write(storage, 0, bytesToWrite);
                                fileWritten = fileWritten + bytesToWrite;
                            }
                        }

                    }
                    downloadFile(outputPath + f.filename, true);
                }
                finally
                {
                    readFile.Close();
                    writeFile.Close();
                    System.IO.File.Delete(inputPath + f.guid); //Delete encrypted file after decrypted
                    delete(f.id);
                }
            }
            else { throw new Exception("Incorrect Password or file no longer exists"); }





        }

        private static string hashString(string password)
        {
            SHA512Managed HashTool = new SHA512Managed();
            Byte[] PhraseAsByte = System.Text.Encoding.UTF8.GetBytes(string.Concat(password));
            Byte[] EncryptedBytes = HashTool.ComputeHash(PhraseAsByte);
            HashTool.Clear();
            return Convert.ToBase64String(EncryptedBytes);
        }

        private static File getByID(int id)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connName].ConnectionString))
                using (var cmd = new SqlCommand("encryption_files_getByID", conn) { CommandType = CommandType.StoredProcedure })
                {
                    conn.Open();
                    cmd.Parameters.Add(new SqlParameter("@id", id));

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    return loadDataIntoObject(dt).FirstOrDefault();
                }
            }
            catch
            {
                return null;
            }

        }

        private static File getByPasswordAndGUID(string passwordHash, string guid)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connName].ConnectionString))
                using (var cmd = new SqlCommand("encryption_files_getByPasswordAndGUID", conn) { CommandType = CommandType.StoredProcedure })
                {
                    conn.Open();
                    cmd.Parameters.Add(new SqlParameter("@passwordHash", passwordHash));
                    cmd.Parameters.Add(new SqlParameter("@guid", guid));

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    return loadDataIntoObject(dt).FirstOrDefault();
                }
            }
            catch
            {
                return null;
            }

        }

        private static void insert(File entity)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connName].ConnectionString))
            using (var cmd = new SqlCommand("encryption_files_insert", conn) { CommandType = CommandType.StoredProcedure })
            {
                conn.Open();
                cmd.Parameters.Add(new SqlParameter("@guid", entity.guid));
                cmd.Parameters.Add(new SqlParameter("@filename", entity.filename));
                cmd.Parameters.Add(new SqlParameter("@passwordHash", entity.passwordHash));

                cmd.ExecuteNonQuery();
            }
        }

        private static void delete(int id)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connName].ConnectionString))
            using (var cmd = new SqlCommand("encryption_files_delete", conn) { CommandType = CommandType.StoredProcedure })
            {
                conn.Open();
                cmd.Parameters.Add(new SqlParameter("@id", id));

                cmd.ExecuteNonQuery();
            }
        }

        private static List<File> loadDataIntoObject(DataTable dt)
        {
            List<File> list = new List<File>();
            foreach (var row in dt.AsEnumerable())
            {
                File obj = new File();

                foreach (var prop in obj.GetType().GetProperties())
                {
                    try
                    {
                        PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                        propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                    }
                    catch { continue; }
                }
                list.Add(obj);
            }

            return list;
        }
    }
    public class File
    {
        public int id { get; set; }
        public string guid { get; set; }
        public string filename { get; set; }
        public string passwordHash { get; set; }

        
    }
        
    
}
