/****** Object:  Table [dbo].[files]    Script Date: 2/6/2017 11:19:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[files](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[guid] [nvarchar](50) NULL,
	[passwordHash] [nvarchar](200) NULL,
	[filename] [nvarchar](100) NULL
) ON [PRIMARY]

GO
/****** Object:  StoredProcedure [dbo].[encryption_files_delete]    Script Date: 2/6/2017 11:19:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[encryption_files_delete]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM files where id = @id
END

GO
/****** Object:  StoredProcedure [dbo].[encryption_files_getByID]    Script Date: 2/6/2017 11:19:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[encryption_files_getByID]
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM files WHERE id = @id
END

GO
/****** Object:  StoredProcedure [dbo].[encryption_files_getByPasswordAndGUID]    Script Date: 2/6/2017 11:19:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[encryption_files_getByPasswordAndGUID]
	-- Add the parameters for the stored procedure here
	@passwordHash nvarchar(200),
	@guid nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM files WHERE passwordHash = @passwordHash and [guid] = @guid
END

GO
/****** Object:  StoredProcedure [dbo].[encryption_files_insert]    Script Date: 2/6/2017 11:19:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[encryption_files_insert]
	-- Add the parameters for the stored procedure here
	@filename nvarchar(100),
	@passwordHash nvarchar(200),
	@guid nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO files (passwordHash, [guid], [filename]) VALUES (@passwordHash, @guid, @filename)
END

GO
